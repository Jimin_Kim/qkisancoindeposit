﻿#ifndef KSCOINMANAGER_H
#define KSCOINMANAGER_H

#include <QObject>
#include <QStringList>
#include <QVector>
#include <QThread>
//#include "qextserialport/qextserialport.h"
#include <QTimer>

#include <KsLibrary.h>


class ReceiveHandler;
class KsCoinManager : public QThread
{
    Q_OBJECT

public:
    explicit KsCoinManager(QObject *parent  = 0);

    bool Connect();
    bool Connect(QString portName_Coin, QString portName_Switch);
    bool Disconnect();

    bool Start();
    bool End();
    bool Reset();

    QList<int> GetCountInfo();
    QStringList GetCoinIdList();
    void GetSystemStatus(QByteArray &statusCodes, QStringList &statusMessages);
    void GetErrorStatus(quint8 &errorCode, QString &errorMessage);
    void GetDoorStatus(quint8 &doorSensor_Bottom, quint8 &doorSensor_Top);
    bool IsRunning();
    bool IsConnected();
    void SetLogFilePath(QString path = "");

protected:
    void run();

public:
    void DirectMessage(const QString& strMsg);

signals:
    void onMessage_signal(QString strMsg);
    void SendPacket_Coin_signal(QByteArray oPacket);
    void SendPacket_Switch_signal(QByteArray oPacket);

public slots:
    void OnRecvData_Coin();
    void OnRecvData_Switch();

private slots:
    void TimerOn();

private:

/* Connection */
    bool ConnectPort();
    bool DisconnectPort();


/* Send */
    void SendPacket_Coin(QByteArray oPacket);
    void SendPacket_Switch(QByteArray oPacket);

    QByteArray SetPacketToSend_Coin(quint8 nCommand, QByteArray oData);
    QByteArray SetPacketToSend_Switch(quint8 nCommand, QByteArray oData);

    quint8 GetChecksum_2sComplement(QByteArray oPacket);
    quint8 GetChecksum_2sComplement_ExcludingChecksum(QByteArray oPacket);
    QByteArray GetCRC16(QByteArray oPacket);
    bool CheckCRC16(QByteArray oPacket);


/* Data Processing */
    void ParsePacket_Coin(QByteArray &oRecvPacket);
    void ParsePacket_Switch(QByteArray &oPacket);

    void ProcessResponseData_Coin(QByteArray oPacket);
    void ProcessResponseData_Switch(QByteArray oPacket);

    void ReadCount(QByteArray data);
    void ReadCoinID(QByteArray data);
    void GetSystemStatusInfo(QByteArray statusCodes);
    QString GetSystemStatus1Message(quint8 status);
    QString GetSystemStatus2Message(quint8 status);
    QString GetSystemStatus3Message(quint8 status);
    QString GetSystemStatus4Message(quint8 status);
    bool IsErrorData(quint8 resultA, quint8 resultB);
    void GetError(quint8 error);
    QString GetErrorMessage(quint8 error);
    void GetDoorStatusInfo();


/* Timer / Sleep */
    void EndTimer();
    void StartTimer();
    void TimerOn_Coin();
    void TimerOn_Switch();
    void Sleep(int msecs);


/*Log*/
    void SaveLog(QString log);


/* ETC. */
    bool IsMachineReady();
    void RequestCoinID();
    void InitializeSystemStatusInfo();


public:
/* TEST */
    void Send_ReadBufferedCreditOrErrorCodes();
    void Send_RequestSystemStatus();
    void Send_ModifyMasterInhibitStatus(qint8 oData);
    void Send_RequestCoinID();

    void Send_LineCheck();
    void Send_CheckDoorStatus();
    void Send_ChangeLedStatus(quint8 ledValue);
    void Send_ResponseEventDoorStatus();


public:
    QextSerialPort* m_SerialPort_Coin;
    QextSerialPort* m_SerialPort_Switch;

    quint32 m_IsOpen_Coin;
    quint32 m_IsOpen_Switch;


private:
    QByteArray m_RecvPacket_Coin;
    QByteArray m_RecvPacket_Switch;

    QTimer* m_StatusTimer;

    quint8 m_LastCommand;

    bool m_IsReady;
    bool m_IsCounting;

    QString m_LogFilePath;
    QString m_LibVersion;


/* Information */
    qint8 m_EventCount;
    QList<int> m_ArrTotalCount;
    QByteArray m_SystemStatusCodes;
    QStringList m_SystemStatusMessages;
    bool m_IsPowerFailureState;
    bool m_IsError;
    quint8 m_ErrorCode;
    QString m_ErrorMessage;
    QStringList m_CoinIdList;

    quint8 m_LedValue;
    bool m_IsDoorOpened;
    quint8 m_DoorSensor1; //Bottom Door
    quint8 m_DoorSensor2; //Top Door (Cover Door)
    bool m_IsDoorOpenedWhenError;
    bool m_IsErrorCheckCompleted;


/* Sequence twist prevention */
    bool m_IsCountStart;
    bool m_IsCountStop;
    bool m_IsReadBufferedCreditOrErrorCodes;
    bool m_IsRequestSystemStatus;
    bool m_IsRequestingCoinId;

    bool m_IsLineCheck;
    bool m_IsCheckDoorStatus;
    bool m_IsChangeLedStatus;
    bool m_IsResponseEventDoorStatus;


/* Command */
    const static quint8 CMD_C_ModifyMasterInhibitStatus = 0xE4;
    const static quint8 CMD_C_ReadBufferedCreditsOrErrorCode = 0xE5;
    const static quint8 CMD_C_RequestSystemStatus = 0x61;
    const static quint8 CMD_C_RequestCoinId = 0xB8;

    const static quint8 CMD_S_LineCheck = 0xA1;
    const static quint8 CMD_S_CheckDoorStatus = 0xF1;
    const static quint8 CMD_S_ChangeLedStatus = 0xF2;
    const static quint8 CMD_S_EventDoorStatus = 0x92; //Response for event


/* LED */
    const static quint8 LED_OFF = 0x00;
    const static quint8 LED_RED = 0x01;
    const static quint8 LED_BLUE = 0x02;
    const static quint8 LED_REDTOGGLE = 0x03;
    const static quint8 LED_BLUETOGGLE = 0x04;


/* Data */
    //const static quint8 ACK = 0x06;
    //const static quint8 NAK = 0x15;
    const static quint8 RESERVED = 0x00;
    const static quint8 NONE = 0x00;


/* Packet Structure */
    //const static int STX = 0x02;
    //const static int ETX = 0x03;

    const static int POS_C_DataLen = 1;
    const static int POS_C_Cmd = 3;

    const static int PACKET_C_MinSize = 5;
    const static int PACKET_S_MinSize = 6;

    const static int POS_S_DataLen = 1;
    const static int POS_S_Cmd = 2;


    QString m_PortName_Coin;
    QString m_PortName_Switch;

    ReceiveHandler* m_ReceiveHandler_Coin;
    ReceiveHandler* m_ReceiveHandler_Switch;

};


// ------------------------------------------------------------------------------------------------------------
class ReceiveHandler : public QObject
{
    Q_OBJECT


private:
    KsCoinManager* m_KsCoinManager;


public:
    explicit ReceiveHandler(KsCoinManager* KsCoinManager) : QObject(0), m_KsCoinManager(KsCoinManager) {}


public slots:
    void SendPacket_Coin(QByteArray oPacket);
    void SendPacket_Switch(QByteArray oPacket);

    void Recv_Coin() { m_KsCoinManager->OnRecvData_Coin(); }
    void Recv_Switch() { m_KsCoinManager->OnRecvData_Switch(); }
};


// ------------------------------------------------------------------------------------------------------------
class Sleep : public QThread
{
public:
    static void msleep(unsigned long msecs) { QThread::msleep(msecs); }
};

#endif // KSCOINMANAGER_H
