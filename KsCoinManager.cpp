﻿#include "KsCoinManager.h"
#include <QStringList>
#include <QDebug>
#include <QVector>
#include <QCoreApplication>
#include <unistd.h>
#include <QFile>
#include <QDateTime>
#include <QDir>

#include <KsMessage.h>
#include <KsFunction.h>


KsCoinManager::KsCoinManager(QObject *parent) : QThread(parent)
{
    m_LibVersion = "1.1";

    m_SerialPort_Coin = 0;
    m_SerialPort_Switch = 0;

    m_IsOpen_Coin = 0;
    m_IsOpen_Switch = 0;

    m_RecvPacket_Coin = 0;
    m_RecvPacket_Switch = 0;

    m_PortName_Coin = "";
    m_PortName_Switch = "";

    m_IsReady = 0;
    m_IsCounting = 0;

    m_LastCommand = 0;
    m_IsCountStart = 0;
    m_IsCountStop = 0;
    m_IsReadBufferedCreditOrErrorCodes = true;
    m_IsRequestSystemStatus = 0;
    m_IsRequestingCoinId = 0;
    m_IsLineCheck = 0;
    m_IsCheckDoorStatus = 0;
    m_IsChangeLedStatus = 0;
    m_IsResponseEventDoorStatus = 0;

    m_EventCount = 0;
    m_ErrorCode = 0;
    m_IsError = 0;
    m_ErrorMessage = "None";
    m_IsErrorCheckCompleted = 0;
    InitializeSystemStatusInfo();
    m_IsPowerFailureState = 0;
    m_CoinIdList.clear();

    m_LedValue = 0;
    m_DoorSensor1 = 0;
    m_DoorSensor2 = 0;
    m_IsDoorOpened = 0;
    m_IsDoorOpenedWhenError = 0;

    m_StatusTimer = 0;
    m_LogFilePath = "";
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::DirectMessage(const QString& strMsg)
{
    //qDebug() << strMsg;
    emit onMessage_signal(strMsg);
}



/* CONNECTION */
//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::Connect()
{
    m_PortName_Coin = KsFunction::GetPortFromID(CTCOIN_PRODUCTID, CTCOIN_VENDORID, CTCOIN_INTERFACE);
    m_PortName_Switch;

    InitializeSystemStatusInfo();
    start();

    int cnt = 0;
    while(true)
    {
        Sleep(500);

        cnt++;
        if (cnt == 20 || (m_IsOpen_Coin && m_IsOpen_Switch))
        {
            break;
        }
    }

    StartTimer();

    return m_IsOpen_Coin && m_IsOpen_Switch;
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::Connect(QString portName_Coin, QString portName_Switch)
{
    SaveLog("Coin Port Name : [%s], Switch Port Name : [%s]", portName_Coin.toStdString().c_str(), portName_Switch.toStdString().c_str());

    m_PortName_Coin = portName_Coin;
    m_PortName_Switch = portName_Switch;

    InitializeSystemStatusInfo();
    start();

    int cnt = 0;
    while(true)
    {
        Sleep(500);

        cnt++;
        if (cnt == 20 || (m_IsOpen_Coin && m_IsOpen_Switch))
        {
            break;
        }
    }

    StartTimer();

    return m_IsOpen_Coin && m_IsOpen_Switch;
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::Disconnect()
{
    EndTimer();

    quit();

    int cnt = 0;
    while (1)
    {
        if (wait(100))
            return true;

        cnt++;
        if (cnt == 3)
        {
            return false;
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::run()
{
    if (ConnectPort() == false)
    {
        //SaveLog("---------------->> run return false");
        return;
    }

    this->exec();

    DisconnectPort();
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::ConnectPort()
{
    SaveLog("=====================================================================");
    SaveLog("KC10 Lib _ v" + m_LibVersion);
    SaveLog("=====================================================================");
    SaveLog("[KsCoinManager] Connect");

    m_IsReady = false;

    // Coin Port
    if (m_SerialPort_Coin == 0)
    {
        m_SerialPort_Coin = new QextSerialPort();
        m_SerialPort_Coin->setPortName(m_PortName_Coin); //Standard COM Port
        m_SerialPort_Coin->setBaudRate(BAUD9600);
        m_SerialPort_Coin->setDataBits(DATA_8);
        m_SerialPort_Coin->setStopBits(STOP_1);
        m_SerialPort_Coin->setParity(PAR_NONE);

        if (m_SerialPort_Coin->open(QIODevice::ReadWrite))
        {
            m_IsOpen_Coin = true;

            m_ReceiveHandler_Coin = new ReceiveHandler(this);
            connect(m_SerialPort_Coin, SIGNAL(readyRead()), m_ReceiveHandler_Coin, SLOT(Recv_Coin()));
            connect(this, SIGNAL(SendPacket_Coin_signal(QByteArray)), m_ReceiveHandler_Coin, SLOT(SendPacket_Coin(QByteArray)));

            m_EventCount = 0;
            if (IsMachineReady() == false)
            {
                disconnect(m_SerialPort_Coin, SIGNAL(readyRead()), this, SLOT(Recv_Coin()));
                delete m_SerialPort_Coin;
                m_SerialPort_Coin = 0;
                m_IsOpen_Coin = false;
                return false;
            }

            m_CoinIdList.clear();
            RequestCoinID();
        }
        else
        {
            SaveLog("Connection Failed - Coin Port");

            delete m_SerialPort_Coin;
            m_SerialPort_Coin = 0;
            m_IsOpen_Coin = false;
            return false;
        }
    }

    // Switch Port
    if (m_SerialPort_Switch == 0)
    {
        m_SerialPort_Switch = new QextSerialPort();
        m_SerialPort_Switch->setPortName(m_PortName_Switch); //Ch B
        m_SerialPort_Switch->setBaudRate(BAUD115200);
        m_SerialPort_Switch->setDataBits(DATA_8);
        m_SerialPort_Switch->setStopBits(STOP_1);
        m_SerialPort_Switch->setParity(PAR_NONE);

        if (m_SerialPort_Switch->open(QIODevice::ReadWrite))
        {
            m_IsOpen_Switch = true;

            m_ReceiveHandler_Switch = new ReceiveHandler(this);
            connect(m_SerialPort_Switch, SIGNAL(readyRead()), m_ReceiveHandler_Switch, SLOT(Recv_Switch()));
            connect(this, SIGNAL(SendPacket_Switch_signal(QByteArray)), m_ReceiveHandler_Switch, SLOT(SendPacket_Switch(QByteArray)));

            m_IsCheckDoorStatus = true;

            SaveLog("Connected");
            return true;
        }
        else
        {
            SaveLog("Connection Failed - Switch Port");

            delete m_SerialPort_Switch;
            m_SerialPort_Switch = 0;
            m_IsOpen_Switch = false;
            return false;
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::DisconnectPort()
{
    SaveLog("[KsCoinManager] Disconnect");

    if (!m_SerialPort_Coin && !m_SerialPort_Switch)
        return false;


    // Coin Port
    m_IsOpen_Coin = false;
    if (m_SerialPort_Coin)
    {
        disconnect(m_SerialPort_Coin, SIGNAL(readyRead()), m_ReceiveHandler_Coin, SLOT(Recv_Coin()));
        disconnect(this, SIGNAL(SendPacket_Coin_signal(QByteArray)), m_ReceiveHandler_Coin, SLOT(SendPacket_Coin(QByteArray)));
        delete m_SerialPort_Coin;

        m_SerialPort_Coin = 0;
    }


    // Switch Port
    m_IsOpen_Switch = false;
    if (m_SerialPort_Switch)
    {
        disconnect(m_SerialPort_Switch, SIGNAL(readyRead()), m_ReceiveHandler_Switch, SLOT(Recv_Switch()));
        disconnect(this, SIGNAL(SendPacket_Switch_signal(QByteArray)), m_ReceiveHandler_Switch, SLOT(SendPacket_Switch(QByteArray)));
        delete m_SerialPort_Switch;

        m_SerialPort_Switch = 0;
    }

    SaveLog("Disconnected\n");
    return true;
}



/* TIMER / SLEEP */
//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::StartTimer()
{
    m_StatusTimer = new QTimer(this);
    connect(m_StatusTimer, SIGNAL(timeout()), this, SLOT(TimerOn()));

    m_StatusTimer->start(400);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::EndTimer()
{
    if (m_StatusTimer != 0)
    {
        m_StatusTimer->stop();
        disconnect(m_StatusTimer, SIGNAL(timeout()), this, SLOT(TimerOn()));
        m_StatusTimer = 0;
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::TimerOn()
{
    TimerOn_Coin();
    TimerOn_Switch();
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::TimerOn_Coin()
{
    QByteArray oPacket;
    QByteArray oData;

    oData.clear();

    if (m_IsRequestingCoinId)
    {
        return;
    }

    if (m_IsCountStart)
    {
        m_IsCountStart = false;

        oData.push_back((char)0x01);
        oPacket = SetPacketToSend_Coin(CMD_C_ModifyMasterInhibitStatus, oData);
        SendPacket_Coin(oPacket);

    }

    else if (m_IsCountStop)
    {
        m_IsCountStop = false;

        oData.push_back((char)0x00);
        oPacket = SetPacketToSend_Coin(CMD_C_ModifyMasterInhibitStatus, oData);
        SendPacket_Coin(oPacket);;
    }

    else if (m_IsReadBufferedCreditOrErrorCodes)
    {
        m_IsReadBufferedCreditOrErrorCodes = false;

        oPacket = SetPacketToSend_Coin(CMD_C_ReadBufferedCreditsOrErrorCode, oData);
        SendPacket_Coin(oPacket);
        m_LastCommand = CMD_C_ReadBufferedCreditsOrErrorCode;

        m_IsRequestSystemStatus = true;
    }

    else if (m_IsRequestSystemStatus)
    {
        m_IsRequestSystemStatus = false;

        oPacket = SetPacketToSend_Coin(CMD_C_RequestSystemStatus, oData);
        SendPacket_Coin(oPacket);
        m_LastCommand = CMD_C_RequestSystemStatus;

        m_IsReadBufferedCreditOrErrorCodes = true;
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::TimerOn_Switch()
{
    QByteArray data;
    QByteArray packet;

    data.clear();

    if (m_IsLineCheck)
    {
        SaveLog(">> Line Check");

        data.push_back((char)NONE);
        packet = SetPacketToSend_Switch(CMD_S_LineCheck, data);
        SendPacket_Switch(packet);

        m_IsLineCheck = false;
    }

    else if (m_IsCheckDoorStatus)
    {
        SaveLog(">> Check Door Status");

        data.push_back((char)NONE);
        packet = SetPacketToSend_Switch(CMD_S_CheckDoorStatus, data);
        SendPacket_Switch(packet);

        m_IsCheckDoorStatus = false;
    }

    else if (m_IsResponseEventDoorStatus)
    {
        SaveLog(">> Event Door Status Response");

        data.push_back((char)ACK);
        data.push_back((char)RESERVED);
        packet = SetPacketToSend_Switch(CMD_S_EventDoorStatus, data);
        SendPacket_Switch(packet);

        m_IsResponseEventDoorStatus = false;
    }

    else if (m_IsChangeLedStatus)
    {
        QString led = "";
        if (m_LedValue == LED_OFF)
            led = "OFF";
        else if (m_LedValue == LED_RED)
            led = "RED";
        else if (m_LedValue == LED_BLUE)
            led = "BLUE";
        else if (m_LedValue == LED_REDTOGGLE)
            led = "RED Toggle";
        else if (m_LedValue == LED_BLUETOGGLE)
            led = "BLUE Toggle";

        SaveLog("LED : " + led);

        data.push_back((char)m_LedValue);
        packet = SetPacketToSend_Switch(CMD_S_ChangeLedStatus, data);
        SendPacket_Switch(packet);

        m_IsChangeLedStatus = false;
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Sleep(int msecs)
{
    for (int idx = 0; idx < 10; idx++)
    {
        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
        Sleep::msleep(msecs/10); //QThread::msleep(msecs/10);
    }
}



/* SEND - COIN */
//------------------------------------------------------------------------------------------------------------------------------------
void ReceiveHandler::SendPacket_Coin(QByteArray oPacket)
{
    int result = m_KsCoinManager->m_SerialPort_Coin->write(oPacket);

    if (result == -1)
    {
        qDebug() << "[EXCEPTION] Send Packet - C\n";
        return;
    }

    m_KsCoinManager->m_SerialPort_Coin->flush();

    QString msg = "[SEND-C] ";
    for (int nIdx = 0; nIdx < oPacket.size(); nIdx++)
    {
        msg += QString().sprintf("%02X ", (quint8)oPacket[nIdx]);
    }
    //qDebug() << msg; //m_KsCoinManager->OnMessage(msg);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::SendPacket_Coin(QByteArray oPacket)
{
    emit SendPacket_Coin_signal(oPacket);
}

//------------------------------------------------------------------------------------------------------------------------------------
QByteArray KsCoinManager::SetPacketToSend_Coin(quint8 nCommand, QByteArray oData)
{
    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //Dst Addr - Default
    oBuffer.push_back((char)oData.size()); //Data Len
    oBuffer.push_back((char)0x01); //Src Addr - Machine
    oBuffer.push_back((char)nCommand); //Cmd

    for (int idx = 0; idx < oData.size(); idx ++)
    {
        oBuffer.push_back((char)oData[idx]); //Data
    }

    oBuffer.push_back((char)GetChecksum_2sComplement(oBuffer)); //Checksum

    return oBuffer;
}



/* SEND - SWITCH */
//------------------------------------------------------------------------------------------------------------------------------------
void ReceiveHandler::SendPacket_Switch(QByteArray oPacket)
{
    int result = m_KsCoinManager->m_SerialPort_Switch->write(oPacket);

    if (result == -1)
    {
        qDebug() << "[EXCEPTION] Send Packet - S\n";
        return;
    }

    m_KsCoinManager->m_SerialPort_Switch->flush();

    QString msg = "[SEND-S] ";
    for (int nIdx = 0; nIdx < oPacket.size(); nIdx++)
    {
        msg += QString().sprintf("%02X ", (quint8)oPacket[nIdx]);
    }
    //qDebug() << msg; //m_KsCoinManager->OnMessage(msg);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::SendPacket_Switch(QByteArray oPacket)
{
    emit SendPacket_Switch_signal(oPacket);
}

//------------------------------------------------------------------------------------------------------------------------------------
QByteArray KsCoinManager::SetPacketToSend_Switch(quint8 nCommand, QByteArray oData)
{
    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //STX
    oBuffer.push_back((char)oData.size()); //Data Len
    oBuffer.push_back((char)nCommand); //CMD

    for (int idx = 0; idx < oData.size(); idx ++)
    {
        oBuffer.push_back((char)oData[idx]); //Data
    }

    QByteArray arrCRC16 = GetCRC16(oBuffer);
    oBuffer.push_back((char)arrCRC16[0]);
    oBuffer.push_back((char)arrCRC16[1]); //CRC16
    oBuffer.push_back((char)0x03); //ETX

    return oBuffer;
}



/* RECEIVE - COIN */
//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::OnRecvData_Coin()
{
    int readSize = (int)m_SerialPort_Coin->size();
    if (readSize <= 0)
        return;

    //qDebug() << QString().sprintf("[OnRecvData-C] Size = %d", readSize);

    QByteArray oData = m_SerialPort_Coin->readAll();
    m_RecvPacket_Coin.append(oData);

    QString msg = "[RECV-C] ";
    for (int nIdx = 0; nIdx < oData.size(); nIdx++)
    {
        msg += QString().sprintf("%02X ", (quint8)oData[nIdx]);
    }
    //qDebug() << msg;

    ParsePacket_Coin(m_RecvPacket_Coin);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::ParsePacket_Coin(QByteArray &oRecvPacket)
{
    QByteArray oRetPacket;
    oRetPacket.clear();

     quint32 nPacketTotalSize = 0;

     while (oRecvPacket.size() > 0)
     {
         if (oRecvPacket.size() >= PACKET_C_MinSize)
         {
             quint32 nDataSize = oRecvPacket[POS_C_DataLen];
             nPacketTotalSize = PACKET_C_MinSize + nDataSize;
             if (oRecvPacket.size() >= nPacketTotalSize)
             {
                 quint8 nChecksum = oRecvPacket[oRecvPacket.size() - 1];
                 if(nChecksum == GetChecksum_2sComplement_ExcludingChecksum(oRecvPacket))
                 {
                     oRetPacket = QByteArray((char*)oRecvPacket.data(), nPacketTotalSize);
                     oRecvPacket.remove(0, nPacketTotalSize);

                     ProcessResponseData_Coin(oRetPacket);
                 }
             }
         }
     }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::ProcessResponseData_Coin(QByteArray oPacket)
{
    QDataStream oStream(&oPacket, QIODevice::ReadOnly);
    oStream.setByteOrder(QDataStream::BigEndian);

    QByteArray oHeader;
    oHeader.resize(4);
    oStream.readRawData((char*)oHeader.data(), oHeader.size());

    quint8 dataLen = oHeader[POS_C_DataLen];

    QByteArray oData;
    oData.clear();
    oData.resize(dataLen);
    oStream.readRawData((char*)oData.data(), oData.size());


    if (m_IsPowerFailureState)
    {
        m_IsOpen_Coin = 0;
        m_IsOpen_Switch = 0;

        m_IsPowerFailureState = false;
    }


    if (dataLen == 21) //if (m_LastCommand == CMD_C_ReadBufferedCreditsOrErrorCode && dataLen == 21)
    {
        ReadCount(oData);
    }

    else if (dataLen == 4) //else if (m_LastCommand == CMD_C_RequestSystemStatus && dataLen == 4)
    {
        GetSystemStatusInfo(oData);
    }

    else if (dataLen == 6) //else if (m_LastCommand == CMD_C_RequestCoinId && dataLen == 6)
    {
        ReadCoinID(oData);
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::GetSystemStatusInfo(QByteArray statusCodes)
{
    QString statusMsg1 = GetSystemStatus1Message((char)statusCodes[0]);
    QString statusMsg2 = GetSystemStatus2Message((char)statusCodes[1]);
    QString statusMsg3 = GetSystemStatus3Message((char)statusCodes[2]);
    QString statusMsg4 = GetSystemStatus4Message((char)statusCodes[3]);
    //qDebug() << statusMsg1 + " " + statusMsg2 + " " + statusMsg3 + " " + statusMsg4;

    QStringList statusMessages;
    statusMessages.push_back(statusMsg1);
    statusMessages.push_back(statusMsg2);
    statusMessages.push_back(statusMsg3);
    statusMessages.push_back(statusMsg4);


    QString msg = "Status : ";
    for (int i = 0; i < 4; i++)
    {
        if (m_SystemStatusMessages[i] != statusMessages[i])
        {
            msg += statusMessages[0] + QString().sprintf("(%02X)", (quint8)statusCodes[0]) + " / ";
            msg += statusMessages[1] + QString().sprintf("(%02X)", (quint8)statusCodes[1]) + " / ";
            msg += statusMessages[2] + QString().sprintf("(%02X)", (quint8)statusCodes[2]) + " / ";
            msg += statusMessages[3] + QString().sprintf("(%02X)", (quint8)statusCodes[3]);

            SaveLog(msg);
            break;
        }
    }

    for (int i = 0; i < 4; i++)
    {
        m_SystemStatusCodes[i] = statusCodes[i];
        m_SystemStatusMessages[i] = statusMessages[i];
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
QString KsCoinManager::GetSystemStatus1Message(quint8 nStatus)
{
    QString statusMsg = "";

    switch (nStatus)
    {
    case 0x01:
        statusMsg = "Motor1 Running";
        if (m_IsCounting == false)
        {
            m_LedValue = LED_BLUE;
            m_IsChangeLedStatus = true;

            m_IsCounting = true;
            SaveLog("Counting : True");
        }
        break;

    case 0x02:
        statusMsg = "Motor2 Running";
        break;

    case 0x04:
        statusMsg = "Motor3 Running";
        break;

    case 0x08:
        statusMsg = "Motor4 Running";
        break;

    case 0x10:
        statusMsg = "Bowl Open";
        break;

    case 0x20:
        statusMsg = "Coins In Bowl";
        break;

    case 0x40:
        statusMsg = "Gate Open";
        break;

    case 0x80:
        statusMsg = "Power Failure State";
        m_IsPowerFailureState = true;
        break;

    default:
        break;
    }

    return statusMsg;
}

//------------------------------------------------------------------------------------------------------------------------------------
QString KsCoinManager::GetSystemStatus2Message(quint8 nStatus)
{
    QString statusMsg = "";

    switch (nStatus)
    {
    case 0x80:
        statusMsg = "Clean Cycle Running";
        break;

    default:
        break;
    }

    return statusMsg;
}

//------------------------------------------------------------------------------------------------------------------------------------
QString KsCoinManager::GetSystemStatus3Message(quint8 nStatus)
{
    QString statusMsg = "";

    switch (nStatus)
    {
    case 0x01:
        statusMsg = "Rail Stop";
        break;

    case 0x40:
        statusMsg = "Sensor Blocked";
        break;

    case 0x80:
        statusMsg = "No Communication To Sensor";
        break;

    default:
        break;
    }

    return statusMsg;
}

//------------------------------------------------------------------------------------------------------------------------------------
QString KsCoinManager::GetSystemStatus4Message(quint8 nStatus)
{
    QString statusMsg = "";

    switch (nStatus)
    {
    case 0x10:
        statusMsg =  "Count Mode";
        break;

    case 0x20:
        statusMsg = "Test Mode";
        break;

    case 0x40:
        statusMsg = "Calibration Mode";
        break;

    case 0x50:
        statusMsg = "Calibration Done Mode";
        break;

    case 0x70:
        statusMsg = "Stop Mode";
        if (m_IsCounting)
        {
            if (m_IsError == false && m_IsDoorOpened == false)
            {
                m_LedValue = LED_OFF;
                m_IsChangeLedStatus = true;
            }
            m_IsCounting = false;
            SaveLog("Counting : False");
        }
        break;

    case 0x90:
        statusMsg = "Store Data Mode";
        break;

    default:
        break;
    }

    return statusMsg;
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::ReadCount(QByteArray data)
{
    quint8 eventCnt = (quint8)data[0];
    QString info = QString().sprintf("(%03d)  %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X | %02X",
                                     (quint8)data[0], (quint8)data[1], (quint8)data[2], (quint8)data[3], (quint8)data[4], (quint8)data[5], (quint8)data[6], (quint8)data[7], (quint8)data[8], (quint8)data[9], (quint8)data[10],
                                     (quint8)data[11], (quint8)data[12], (quint8)data[13], (quint8)data[14], (quint8)data[15], (quint8)data[16], (quint8)data[17], (quint8)data[18], (quint8)data[19], (quint8)data[20]);

    if (m_IsReady) { SaveLog(info); }
    else if (eventCnt != m_EventCount) { SaveLog(info); }
    qDebug() << info;

    int nIdx = 0;
    if (m_IsReady == false)
    {
        if (m_EventCount == 0)
        {
            m_EventCount = data[nIdx];
            return;
        }
    }

    if (eventCnt == m_EventCount)
    {
        return;
    }

    nIdx++;

    quint8 skippedCount = eventCnt - m_EventCount;
    if (skippedCount < 0) //Event Count : 0(00) ~ 255(FF)
    {
        skippedCount += 255;
    }

    quint8 resultA = 0x00;
    quint8 resultB = 0x00;

    for (int i = 0; i < skippedCount; i++)
    {
        if (i >= 10) //Hold the last 10 events.
        {
            SaveLog(QString("Excessive Event Count! Event Count Gap : %1").arg(skippedCount));
            break;
        }

        resultA = data[nIdx++];
        resultB = data[nIdx++];

        if (IsErrorData(resultA, resultB) == false)
        {
            if (resultB == 0x00)
                m_ArrTotalCount[resultA - 1]++;
        }
    }

    QString countingInfo = "Counting Info : ";
    for (int i = 0; i < m_ArrTotalCount.size(); i++)
    {
        countingInfo += QString("%1").arg(m_ArrTotalCount[i]);
        if (i < m_ArrTotalCount.size() - 1) countingInfo += " | ";
    }
    SaveLog(countingInfo);

    m_EventCount = eventCnt;
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::IsErrorData(quint8 resultA, quint8 resultB)
{
    if (resultA == 0x00)
    {
        if (resultB == 0x01) // Coin Rejected (Result A: 0x00, Result B: 0x01)
        {
            m_ArrTotalCount[m_ArrTotalCount.size() - 1]++;
            return true;
        }

        if (resultB == 0xA2)
        {
            return true;
        }

        GetError(resultB);
        return true;
    }
    else
    {
        return false;
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::GetError(quint8 error)
{
    QString errorMsg = GetErrorMessage(error);

    if (error == 0x00)
    {
        if (m_IsError)
        {
            if (m_IsReady || m_IsCounting)
            {
                m_LedValue = LED_BLUE;
                m_IsChangeLedStatus = true;
            }

            if (!m_IsReady)
            {
                m_LedValue = LED_OFF;
                m_IsChangeLedStatus = true;
            }

            m_IsError = false;
            SaveLog("Error : None");
        }
    }
    else
    {
        m_LedValue = LED_REDTOGGLE;
        m_IsChangeLedStatus = true;

        m_IsError = true;

        QString msg = QString().sprintf("Error Code : %02X, ", error);
        msg += "Error Message : " + errorMsg;
        SaveLog(msg);
    }

    m_ErrorCode = error;
    m_ErrorMessage = errorMsg;
}

//------------------------------------------------------------------------------------------------------------------------------------
QString KsCoinManager::GetErrorMessage(quint8 error)
{
    QString errorMsg = "None"; //0x00

    switch (error)
    {
    case 0x01:
        errorMsg = "CoinRejected";
        break;

    case 0x02:
        errorMsg = "Inhibited Coin";
        break;

    case 0x03:
        errorMsg = "Mutiple Windows";
        break;

    case 0x09:
        errorMsg = "Sensor Main Unknown State";
        break;

    case 0x0A:
        errorMsg = "Sensor Coin Unknown State";
        break;

    case 0x0B:
        errorMsg = "Sensor Not Ready";
        break;

    case 0x0C:
        errorMsg = "Reject Coin Not Cleared / Sensor Blocked";
        break;

    case 0x0D:
        errorMsg = "Validation Sensor Not Ready";
        break;

    case 0x0E:
        errorMsg = "Sensor Blocked";
        break;

    case 0x0F:
        errorMsg = "Rail Stop";
        break;

   case 0x18:
       errorMsg = "Reject Coin Repeat";
       break;

   case 0x19:
       errorMsg = "Reject Slug";
       break;

   case 0x1A:
       errorMsg = "Reject Sensor Blocked";
       break;

   case 0x21:
       errorMsg = "Supply Voltage Outside Operating Limits";
       break;

   case 0x23:
       errorMsg = "Motor Exception";
       break;

   case 0xA1:
       errorMsg = "Bowl Open";
       break;

    case 0xA2:
       errorMsg = "Beginning Save Mode";
       break;

    case 0xA3:
       errorMsg = "Exiting Save Mode";
       break;

    case 0xB4:
       errorMsg = "Host Timeout";
       break;

    default:
       errorMsg = "Unknown";
       break;
    }

    return errorMsg;
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::ReadCoinID(QByteArray data)
{
    if (data.size() != 6)
        return;


    QString coinData = "";
    for (int nIdx = 0; nIdx < data.size(); nIdx++)
    {
        coinData += (char)data[nIdx];
    }

    if (coinData != "......" && coinData != "      ")
    {
        m_CoinIdList.push_back(coinData);
    }

    DirectMessage(coinData);
}



/* RECEIVE - SWITCH */
//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::OnRecvData_Switch()
{
    int readSize = (int)m_SerialPort_Switch->size();
    if (readSize <= 0)
        return;

    //qDebug() << QString().sprintf("[OnRecvData_S] Size = %d", readSize);

    QByteArray oData = m_SerialPort_Switch->readAll();
    m_RecvPacket_Switch.append(oData);

    QString msg = "[RECV-S] ";
    for (int nIdx = 0; nIdx < oData.size(); nIdx++)
    {
        msg += QString().sprintf("%02X ", (quint8)oData[nIdx]);
    }
    //qDebug() << msg;

    ParsePacket_Switch(m_RecvPacket_Switch);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::ParsePacket_Switch(QByteArray &oPacket)
{
     QByteArray oRetPacket = 0;

     quint32 nPacketTotalSize = 0; //Fix Size : 8

     while (oPacket.size() > 0)
     {
         if (oPacket.size() >= PACKET_S_MinSize)
         {
             quint32 nDataSize = oPacket[POS_S_DataLen];
             nPacketTotalSize = PACKET_S_MinSize + nDataSize;

             if (oPacket.size() >= nPacketTotalSize)
             {
                 if (oPacket[0] == STX && oPacket[nPacketTotalSize - 1] == ETX)
                 {
                     if (CheckCRC16(oPacket))
                     {
                         oRetPacket = QByteArray((char*)oPacket.data(), nPacketTotalSize);
                         oPacket.remove(0, nPacketTotalSize);

                         ProcessResponseData_Switch(oRetPacket);
                     }
                 }
             }
         }
     }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::ProcessResponseData_Switch(QByteArray oPacket)
{
    QDataStream oStream(&oPacket, QIODevice::ReadOnly);
    oStream.setByteOrder(QDataStream::BigEndian);

    QByteArray oHeader;
    oHeader.resize(3);
    oStream.readRawData((char*)oHeader.data(), oHeader.size());

    quint8 dataLen = oHeader[POS_S_DataLen];
    quint8 command = oHeader[POS_S_Cmd];

    QByteArray oData;
    oData.resize(dataLen);
    oStream.readRawData((char*)oData.data(), oData.size());

    switch(command)
    {
    case 0xA1:
        if (oData[0] == ACK)
        {
            DirectMessage("Line Check : ACK\n");
            m_IsCheckDoorStatus = true;
        }
        break;

    case 0xF1:
        m_DoorSensor1 = oData[0];
        m_DoorSensor2 = oData[1];

        GetDoorStatusInfo();
        m_IsChangeLedStatus = true;
        break;

    case 0xF2:
        if (oData[0] == ACK)
        {
            DirectMessage("Change LED Status : ACK\n");
        }
        break;

    case 0x92:
        //DirectMessage("Event Door Status (0x92)\n");

        m_DoorSensor1 = oData[0];
        m_DoorSensor2 = oData[1];

        // Exception Processing
        if (m_DoorSensor1 == NAK && m_DoorSensor2 == 0xF3)
        {
            m_IsLineCheck = true;
            return;
        }

        m_IsResponseEventDoorStatus = true;
        GetDoorStatusInfo();
        break;

    default:
        break;
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::GetDoorStatusInfo()
{
    quint8 CLOSED = 0x00;
    quint8 OPEN = 0x01;

    if (m_DoorSensor1 == CLOSED && m_DoorSensor2 == CLOSED)
    {
        SaveLog("Door 1: Closed, Door 2: Closed");
    }
    else if (m_DoorSensor1 == OPEN && m_DoorSensor2 == OPEN)
    {
        SaveLog("Door 1: Open, Door 2: Open");
    }
    else if (m_DoorSensor1 == CLOSED && m_DoorSensor2 == OPEN)
    {
        SaveLog("Door 1: Closed, Door 2: Open");
    }
    else if (m_DoorSensor1 == OPEN && m_DoorSensor2 == CLOSED)
    {
        SaveLog("Door 1: Open, Door 2: Closed");
    }

    if (m_DoorSensor1 == CLOSED && m_DoorSensor2 == CLOSED)
    {
        m_IsDoorOpened = false;

        if (m_ErrorCode != 0x00)
            m_LedValue = LED_REDTOGGLE;
        else
        {
            if (!m_IsReady)
                m_LedValue = LED_OFF;

            else
                m_LedValue = LED_BLUE;

        }
        m_IsChangeLedStatus = true;

        if (m_IsDoorOpenedWhenError)
        {
            m_IsErrorCheckCompleted = true;
            m_IsDoorOpenedWhenError = false;
        }
    }
    else if (m_DoorSensor1 == OPEN || m_DoorSensor2 == OPEN)
    {
        m_IsDoorOpened = true;

        m_LedValue = LED_RED;
        m_IsChangeLedStatus = true;

        if (m_IsError && m_DoorSensor2 == OPEN)
        {
            m_IsDoorOpenedWhenError = true;
        }
    }
}



/* CHECKSUM / CRC16 */
//------------------------------------------------------------------------------------------------------------------------------------
quint8 KsCoinManager::GetChecksum_2sComplement(QByteArray oPacket)
{
    quint8 checksum = 0x00;

    for (int nIdx = 0; nIdx < oPacket.size(); nIdx++)
    {
        checksum += oPacket[nIdx];
    }

    checksum = checksum % 256;
    checksum = 256 - checksum;

    return checksum;
}

//------------------------------------------------------------------------------------------------------------------------------------
quint8 KsCoinManager::GetChecksum_2sComplement_ExcludingChecksum(QByteArray oPacket)
{
    quint8 checksum = 0x00;

    for (int nIdx = 0; nIdx < oPacket.size() - 1; nIdx++)
    {
        checksum += oPacket[nIdx];
    }

    checksum = checksum % 256;
    checksum = 256 - checksum;

    return checksum;
}

//------------------------------------------------------------------------------------------------------------------------------------
QByteArray KsCoinManager::GetCRC16(QByteArray oPacket)
{
    ushort _xtemp = 0;
    ushort _crc_checksum = 0xFFFF;

    for (int index = 1; index < oPacket.length(); index++) //Data Len ~ Data
    {
        _xtemp = (ushort)(_crc_checksum ^ (oPacket[index] & 0xff));

        for (int _for_j = 0; _for_j < 8; _for_j++)
        {
            if ((_xtemp & 1) >= 1)
            {
                _xtemp >>= 1;
                _xtemp ^= 0xA001;
            }
            else
            {
                _xtemp >>= 1;
            }
        }
        _crc_checksum = _xtemp;
    }

    QByteArray result;

    char CRC_1 = _crc_checksum & 0x00FF; // 0x00FF
    char CRC_2 = _crc_checksum >> 8; //0x00ff

    result.push_back(CRC_1);
    result.push_back(CRC_2);

    return result;
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::CheckCRC16(QByteArray oPacket)
{
    quint32 headerSize = 3;
    quint32 dataSize = oPacket[1];
    quint32 crcPosition = oPacket.size() - 3;

    QDataStream oStream(&oPacket, QIODevice::ReadOnly);
    oStream.setByteOrder(QDataStream::BigEndian);

    QByteArray oTempBuffer; //STX ~ Data
    oTempBuffer.resize(headerSize + dataSize);
    oStream.readRawData((char*)oTempBuffer.data(), oTempBuffer.size());

    QByteArray arrCRC16 = GetCRC16(oTempBuffer);
    if (oPacket[crcPosition] == arrCRC16[0] && oPacket[crcPosition + 1] == arrCRC16[1])
    {
        return true;
    }
    else
    {
        return false;
    }
}



/* ETC. */
//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::IsMachineReady()
{
    quint32 cnt = 0;
    while(true)
    {
        Send_ReadBufferedCreditOrErrorCodes();
        Sleep(500);

        DirectMessage("KC10 Waiting...");
        if (m_EventCount != 0)
        {
            return true;
        }

        cnt++;
        DirectMessage("Retry Connection Count %d", cnt);

        if (cnt == 3)
        {
            SaveLog("Connection Timeout");
            return false;
        }
    }
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::RequestCoinID()
{
    DirectMessage("\n>> Request Coin ID");

    QByteArray oData;
    QByteArray oPacket;

    m_IsRequestingCoinId = true;

    for (int coinIdx = 1; coinIdx <= 30; coinIdx++)
    {
        oData.clear();
        oData.push_back((char)coinIdx);

        oPacket.clear();
        oPacket = SetPacketToSend_Coin(CMD_C_RequestCoinId, oData);

        SendPacket_Coin(oPacket);
        m_LastCommand = 0xB8;

        Sleep(200);
    }

    m_IsRequestingCoinId = false;

    QString coinIdInfo = "Coin Id: ";
    for (int i = 0; i < m_CoinIdList.count(); i++)
    {
        coinIdInfo += QString("%1").arg(m_CoinIdList[i]);
        if (i < m_CoinIdList.count() - 1) coinIdInfo += " | ";
    }
    SaveLog(coinIdInfo);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::InitializeSystemStatusInfo()
{
    m_SystemStatusCodes.clear();
    m_SystemStatusMessages.clear();

    for (int i = 0; i < 4; i++)
    {
        m_SystemStatusCodes.push_back((char)0x00);
        m_SystemStatusMessages.push_back("None");
    }
}



/* LOG */
//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::SaveLog(QString log)
{
    DirectMessage(log);

    if (m_LogFilePath != "")
    {
        QString fileName = "Log_" + QDateTime::currentDateTime().toString("yyyy_MM_dd") + ".txt";
        QString filePath = m_LogFilePath + QDir::separator() + fileName;

        QFile logFile(filePath);
        if (logFile.open(QIODevice::Append | QIODevice::Text) == false)
        {
            return;
        }

        QTextStream stream(&logFile);
        QString curDateTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss:zzz");
        stream << "[" << curDateTime << "]" << "\t" << log << "\n";
        logFile.close();
    }
}



/* LIB */
//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::Start()
{
    SaveLog("[KsCoinManager] Start");

    if (!m_SerialPort_Coin || !m_SerialPort_Switch)
        return false;

    if (m_IsReady)
        return false;

    if (m_IsCounting)
        return false;

//    if (m_IsDoorOpened)
//        return false;

    Sleep(500);
    m_IsReady = true;
    SaveLog("Ready : True");

    m_ArrTotalCount.clear();
    for (int i = 0; i < m_CoinIdList.size() + 1; i++) m_ArrTotalCount.append(0);

    m_IsCountStart = true;

    return true;
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::End()
{
    SaveLog("[KsCoinManager] End");

    if (m_IsReady == false)
    {
        return false;
    }

    m_IsCountStop = true;

    Sleep(500);
    m_IsReady = false;
    SaveLog("Ready : False");

    return true;
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::Reset()
{
    SaveLog("[KsCoinManager] Reset");

    //if (m_IsErrorCheckCompleted)
    //{
        m_IsReady = false;
        SaveLog("Ready : False");

        GetError(0x00); //Error.None

        SaveLog("Reset : Succeeded");
        m_IsErrorCheckCompleted = false;
        return true;
    //}
    //else
    //{
    //    SaveLog("Reset : Failed");
    //    return false;
    //}
}

//------------------------------------------------------------------------------------------------------------------------------------
QList<int> KsCoinManager::GetCountInfo()
{
    QString strCountingInfo = "[KsCoinManager] Get Count Info : ";
    for (int i = 0; i < m_ArrTotalCount.size(); i++)
    {
        strCountingInfo += QString("%1").arg(m_ArrTotalCount[i]);
        if (i < m_ArrTotalCount.size() - 1) strCountingInfo += " | ";
    }
    SaveLog(strCountingInfo);

    return m_ArrTotalCount;
}

//------------------------------------------------------------------------------------------------------------------------------------
QStringList KsCoinManager::GetCoinIdList()
{
    QString strCoinIdList = "[KsCoinManager] Get Coin Id List : ";
    for (int i = 0; i < m_CoinIdList.count(); i++)
    {
        strCoinIdList += QString("%1").arg(m_CoinIdList[i]);
        if (i < m_CoinIdList.count() - 1) strCoinIdList += " | ";
    }
    SaveLog(strCoinIdList);

    return m_CoinIdList;
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::GetSystemStatus(QByteArray &statusCodes, QStringList &statusMessages)
{
    for (int i = 0; i < 4; i++)
    {
        statusCodes.push_back(m_SystemStatusCodes[i]);
        statusMessages.push_back(m_SystemStatusMessages[i]);
    }

    QString strSystemStatus = "[KsCoinManager] Get System Status : ";
    strSystemStatus += statusMessages[0] + QString().sprintf("(%02X)", (quint8)statusCodes[0]) + " / ";
    strSystemStatus += statusMessages[1] + QString().sprintf("(%02X)", (quint8)statusCodes[1]) + " / ";
    strSystemStatus += statusMessages[2] + QString().sprintf("(%02X)", (quint8)statusCodes[2]) + " / ";
    strSystemStatus += statusMessages[3] + QString().sprintf("(%02X)", (quint8)statusCodes[3]);
    SaveLog(strSystemStatus);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::GetErrorStatus(quint8 &errorCode, QString &errorMessage)
{
    errorCode = m_ErrorCode;
    errorMessage = m_ErrorMessage;

    QString strErrorStatus = "[KsCoinManager] Get Error Status : ";
    strErrorStatus += errorMessage;
    strErrorStatus += QString().sprintf("(%02X)",(quint8)errorCode);
    SaveLog(strErrorStatus);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::GetDoorStatus(quint8 &doorSensor_Bottom, quint8 &doorSensor_Top)
{
    doorSensor_Bottom = m_DoorSensor1; //door
    doorSensor_Top = m_DoorSensor2; //cover door

    QString doorInfo = "[KsCoinManager] Get Door Status - ";
    if (m_DoorSensor1 == 0x00 && m_DoorSensor2 == 0x00)
        doorInfo += "Door 1: Closed, Door 2: Closed";
    else if (m_DoorSensor1 == 0x00 && m_DoorSensor2 == 0x01)
        doorInfo += "Door 1: Closed, Door 2: Open";
    else if (m_DoorSensor1 == 0x01 && m_DoorSensor2 == 0x00)
        doorInfo += "Door 1: Open, Door 2: Closed";
    else if (m_DoorSensor1 == 0x01 && m_DoorSensor2 == 0x01)
        doorInfo += "Door 1: Open, Door 2: Open";

    SaveLog(doorInfo);
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::IsRunning()
{
    if (m_IsCounting)
        SaveLog("[KsCoinManager] Is Running : True");
    else
        SaveLog("[KsCoinManager] Is Running : False");

    return m_IsCounting;
}

//------------------------------------------------------------------------------------------------------------------------------------
bool KsCoinManager::IsConnected()
{
    if (m_IsOpen_Coin && m_IsOpen_Switch)
        SaveLog("[KsCoinManager] Is Connected : True");
    else
        SaveLog("[KsCoinManager] Is Connected : False");

    return m_IsOpen_Coin && m_IsOpen_Switch;
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::SetLogFilePath(QString path/* = "" */)
{
    SaveLog("[KsCoinManager] Set Log File Path : " + path);

    if (path != "")
    {
        QDir logFileDir(m_LogFilePath);
        if (!logFileDir.exists())
        {
            logFileDir.mkpath(m_LogFilePath);
        }

        path = QDir::toNativeSeparators(path);
    }

    m_LogFilePath = path;
}



/* TEST */
//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Send_ModifyMasterInhibitStatus(qint8 oInhibitStatusData)
{
    if (oInhibitStatusData == 0x00)
        DirectMessage("\n>> Modify Master Inhibit Status - STOP");

    else if (oInhibitStatusData == 0x01)
        DirectMessage("\n>> Modify Master Inhibit Status - START");

    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //Dst Addr - Default
    oBuffer.push_back((char)0x01); //Data Len
    oBuffer.push_back((char)0x01); //Src Addr - Machine
    oBuffer.push_back((char)0xE4); //Cmd

    oBuffer.push_back((char)oInhibitStatusData); //Data

    oBuffer.push_back((char)GetChecksum_2sComplement(oBuffer)); //Checksum

    m_LastCommand = 0xE4;
    SendPacket_Coin(oBuffer);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Send_ReadBufferedCreditOrErrorCodes()
{
    DirectMessage("\n>> Read Buffered Credit Or Error Codes");

    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //Dst Addr - Default
    oBuffer.push_back((char)0x00); //Data Len
    oBuffer.push_back((char)0x01); //Src Addr - Machine
    oBuffer.push_back((char)0xE5); //Cmd
    oBuffer.push_back((char)GetChecksum_2sComplement(oBuffer)); //Checksum

    m_LastCommand = 0xE5;
    SendPacket_Coin(oBuffer);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Send_RequestSystemStatus()
{
    DirectMessage("\n>> Request System Status");

    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //Dst Addr - Default
    oBuffer.push_back((char)0x00); //Data Len
    oBuffer.push_back((char)0x01); //Src Addr - Machine
    oBuffer.push_back((char)0x61); //Cmd
    oBuffer.push_back((char)GetChecksum_2sComplement(oBuffer)); //Checksum

    m_LastCommand = 0x61;
    SendPacket_Coin(oBuffer);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Send_RequestCoinID()
{
    DirectMessage("\n>> Request Coin ID");

    m_IsRequestingCoinId = true;

    for (int coinIdx = 1; coinIdx <= 30; coinIdx++)
    {
        QByteArray oBuffer;
        oBuffer.push_back((char)0x02); //Dst Addr - Default
        oBuffer.push_back((char)0x01); //Data Len
        oBuffer.push_back((char)0x01); //Src Addr - Machine
        oBuffer.push_back((char)0xB8); //Cmd

        oBuffer.push_back((char)coinIdx); //Data

        oBuffer.push_back((char)GetChecksum_2sComplement(oBuffer)); //Checksum

        m_LastCommand = 0xB8;
        SendPacket_Coin(oBuffer);

        Sleep(200);
    }

    m_IsRequestingCoinId = false;
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Send_LineCheck()
{
    DirectMessage("\n>> Line Check");

    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //STX
    oBuffer.push_back((char)0x01); //Data Len
    oBuffer.push_back((char)0xA1); //CMD

    oBuffer.push_back((char)NONE); //Data

    QByteArray arrCRC16 = GetCRC16(oBuffer);
    oBuffer.push_back((char)arrCRC16[0]);
    oBuffer.push_back((char)arrCRC16[1]); //CRC16
    oBuffer.push_back((char)0x03); //ETX

    SendPacket_Switch(oBuffer);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Send_CheckDoorStatus()
{
    DirectMessage("\n>> Check Door Status");

    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //STX
    oBuffer.push_back((char)0x01); //Data Len
    oBuffer.push_back((char)0xF1); //CMD

    oBuffer.push_back((char)NONE); //Data

    QByteArray arrCRC16 = GetCRC16(oBuffer);
    oBuffer.push_back((char)arrCRC16[0]);
    oBuffer.push_back((char)arrCRC16[1]); //CRC16
    oBuffer.push_back((char)0x03); //ETX

    SendPacket_Switch(oBuffer);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Send_ChangeLedStatus(quint8 ledValue)
{
    DirectMessage("\n>> Change Led Status");

    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //STX
    oBuffer.push_back((char)0x01); //Data Len
    oBuffer.push_back((char)0xF2); //CMD

    oBuffer.push_back((char)ledValue); //Data

    QByteArray arrCRC16 = GetCRC16(oBuffer);
    oBuffer.push_back((char)arrCRC16[0]);
    oBuffer.push_back((char)arrCRC16[1]); //CRC16
    oBuffer.push_back((char)0x03); //ETX

    SendPacket_Switch(oBuffer);
}

//------------------------------------------------------------------------------------------------------------------------------------
void KsCoinManager::Send_ResponseEventDoorStatus()
{
    QByteArray oBuffer;
    oBuffer.push_back((char)0x02); //STX
    oBuffer.push_back((char)0x02); //Data Len
    oBuffer.push_back((char)0x92); //CMD

    oBuffer.push_back((char)ACK);
    oBuffer.push_back((char)RESERVED);//Data

    QByteArray arrCRC16 = GetCRC16(oBuffer);
    oBuffer.push_back((char)arrCRC16[0]);
    oBuffer.push_back((char)arrCRC16[1]); //CRC16
    oBuffer.push_back((char)0x03); //ETX

    SendPacket_Switch(oBuffer);
}
