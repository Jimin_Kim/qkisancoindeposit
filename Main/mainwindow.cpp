#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "CoinDeposit.h"
#include <QDebug>


//------------------------------------------------------------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->pushButton_Send_E5->setStyleSheet("text-align:left;");
    ui->pushButton_Send_61->setStyleSheet("text-align:left;");
    ui->pushButton_Send_B8->setStyleSheet("text-align:left;");
    ui->pushButton_Send_A1->setStyleSheet("text-align:left;");
    ui->pushButton_Send_F1->setStyleSheet("text-align:left;");

    m_CoinDeposit =  new CoinDeposit();
    connect(m_CoinDeposit, SIGNAL(onMessage_signal(QString)), this, SLOT(onMessage(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}


//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Connect_clicked()
{
    if (m_CoinDeposit->IsConnected())
        return;

    m_CoinDeposit->SetLogFilePath("C:\\KC10Lib");

    if (!m_CoinDeposit->Connect("COM11", "COM16")) //Connect("COM12", "COM16")) //Connect(Standard ComPort, Ch B ComPort)
        onMessage("Connection Failed\n");
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Disconnect_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    if (!m_CoinDeposit->Disconnect())
        onMessage("Disconnection Failed\n");
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Start_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    m_CoinDeposit->Start();
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_End_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    m_CoinDeposit->End();
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Reset_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    m_CoinDeposit->Reset();
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_GetCountInfo_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    QList<int> countingInfo = m_CoinDeposit->GetCountInfo();

    QString msg = "Counting Info: ";
    for (int i = 0; i < countingInfo.size(); i++)
    {
        msg += QString("%1").arg(countingInfo[i]);
        if (i < countingInfo.size() - 1)
            msg += " | ";
    }
    onMessage(msg + "\n");
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_GetCoinIdList_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    QString msg = "Coin ID List: ";
    QStringList coinIdList = m_CoinDeposit->GetCoinIdList();
    for (int i = 0; i < coinIdList.size(); i++)
    {
        msg += coinIdList[i];
        msg += "  ";
    }
    onMessage(msg + "\n");
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_GetSystemStatus_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    QByteArray statusCodes;
    QStringList statusMessages;
    statusCodes.clear();
    statusMessages.clear();
    m_CoinDeposit->GetSystemStatus(statusCodes, statusMessages);

    for (int i = 0; i < 4; i++)
    {
        QString msg = QString().sprintf("Status Code: 0x%02X, ", (char)statusCodes[i]);
        msg += "Status Message: " + statusMessages[i];
        onMessage(msg);
    }
    onMessage("");
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_GetErrorStatus_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    quint8 errorCode = 0x00;
    QString errorMessage = "";
    m_CoinDeposit->GetErrorStatus(errorCode, errorMessage);

    QString msg = QString().sprintf("Error Code: %02X, ", errorCode);
    msg += "Error Message: " + errorMessage + "\n";
    onMessage(msg);
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_GetDoorStatus_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    quint8 doorSensor1 = 0x00;
    quint8 doorSensor2 = 0x00;
    m_CoinDeposit->GetDoorStatus(doorSensor1, doorSensor2);

    if (doorSensor1 == 0x00 && doorSensor2 == 0x00)
        onMessage("Door 1: Closed, Door 2: Closed\n");
    else if (doorSensor1 == 0x00 && doorSensor2 == 0x01)
        onMessage("Door 1: Closed, Door 2: Open\n");
    else if (doorSensor1 == 0x01 && doorSensor2 == 0x00)
        onMessage("Door 1: Open, Door 2: Closed\n");
    else if (doorSensor1 == 0x01 && doorSensor2 == 0x01)
        onMessage("Door 1: Open, Door 2: Open\n");
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_IsRunning_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    bool isRunning = m_CoinDeposit->IsRunning();
    if (isRunning)
        onMessage("Running: TRUE\n");
    else
        onMessage("Running: FALSE\n");
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_IsConnected_clicked()
{
    bool isConnected = m_CoinDeposit->IsConnected();
    if (isConnected)
        onMessage("Connected: TRUE\n");
    else
        onMessage("Connected: FALSE\n");
}





/* UI */
//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::onMessage(QString strMsg)
{
    ui->textEdit_Log->append(strMsg);
    ui->textEdit_Log->moveCursor(QTextCursor::End);
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_ClearLog_clicked()
{
    ui->textEdit_Log->clear();
}





/* TEST */
//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_E4_Start_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    qint8 activated = 0x01;
    m_CoinDeposit->Send_ModifyMasterInhibitStatus((char)activated);
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_E4_Stop_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    qint8 deactivated = 0x00;
    m_CoinDeposit->Send_ModifyMasterInhibitStatus((char)deactivated);
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_E5_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    m_CoinDeposit->Send_ReadBufferedCreditOrErrorCodes();
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_61_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    m_CoinDeposit->Send_RequestSystemStatus();
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_B8_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    m_CoinDeposit->Send_RequestCoinID();
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_A1_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    m_CoinDeposit->Send_LineCheck();
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_F1_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    m_CoinDeposit->Send_CheckDoorStatus();
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_F2_Off_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    quint8 LED_Off = 0x00;
    m_CoinDeposit->Send_ChangeLedStatus(LED_Off);
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_F2_Red_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    quint8 LED_Red = 0x01;
    m_CoinDeposit->Send_ChangeLedStatus(LED_Red);
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_F2_Blue_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    quint8 LED_Blue = 0x02;
    m_CoinDeposit->Send_ChangeLedStatus(LED_Blue);
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_F2_RedToggle_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    quint8 LED_RedToggle = 0x03;
    m_CoinDeposit->Send_ChangeLedStatus(LED_RedToggle);
}

//------------------------------------------------------------------------------------------------------------------------------------
void MainWindow::on_pushButton_Send_F2_BlueToggle_clicked()
{
    if (m_CoinDeposit->IsConnected() == false)
        return;

    quint8 LED_BlueToggle = 0x04;
    m_CoinDeposit->Send_ChangeLedStatus(LED_BlueToggle);
}
