#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class CoinDeposit;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    
private slots:
    void onMessage(QString strMsg);

    void on_pushButton_ClearLog_clicked();

    void on_pushButton_Connect_clicked();
    void on_pushButton_Disconnect_clicked();
    void on_pushButton_Start_clicked();
    void on_pushButton_End_clicked();
    void on_pushButton_Reset_clicked();

    void on_pushButton_GetCountInfo_clicked();
    void on_pushButton_GetCoinIdList_clicked();
    void on_pushButton_GetErrorStatus_clicked();
    void on_pushButton_GetDoorStatus_clicked();
    void on_pushButton_IsRunning_clicked();
    void on_pushButton_IsConnected_clicked();
    void on_pushButton_GetSystemStatus_clicked();

    void on_pushButton_Send_E5_clicked();
    void on_pushButton_Send_61_clicked();
    void on_pushButton_Send_B8_clicked();
    void on_pushButton_Send_E4_Start_clicked();
    void on_pushButton_Send_E4_Stop_clicked();
    void on_pushButton_Send_A1_clicked();
    void on_pushButton_Send_F1_clicked();
    void on_pushButton_Send_F2_Off_clicked();
    void on_pushButton_Send_F2_Red_clicked();
    void on_pushButton_Send_F2_Blue_clicked();
    void on_pushButton_Send_F2_RedToggle_clicked();
    void on_pushButton_Send_F2_BlueToggle_clicked();


private:
    Ui::MainWindow *ui;
    CoinDeposit* m_CoinDeposit;
};

#endif // MAINWINDOW_H
