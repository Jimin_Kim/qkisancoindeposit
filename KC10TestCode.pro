#-------------------------------------------------
#
# Project created by QtCreator 2022-11-22T13:33:56
#
#-------------------------------------------------

QT       += core gui network

TARGET = KC10TestCode
TEMPLATE = app


SOURCES += Main/mainwindow.cpp \
    Main/main.cpp \
    qextserialport/qextserialport_win.cpp \
    qextserialport/qextserialport.cpp \
    qextserialport/qextserialenumerator_win.cpp \
    qextserialport/qextserialenumerator.cpp \
    CoinDeposit.cpp \
    KsCoinManager.cpp

HEADERS  += Main/mainwindow.h \
    qextserialport/qextserialport_p.h \
    qextserialport/qextserialport_global.h \
    qextserialport/qextserialport.h \
    qextserialport/qextserialenumerator_p.h \
    qextserialport/qextserialenumerator.h \
    CoinDeposit.h \
    KsCoinManager.h

FORMS    += Main/mainwindow.ui

win32:LIBS             += -lsetupapi -ladvapi32 -luser32
